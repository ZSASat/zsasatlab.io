import { STATUS_CODES } from "http";

export const state = () => ({
    data: {
        'TEMP': [],
        'GPS': [],
        'AIR': [],
        'ACC': [],
        'RSSI': [],
        }
  })
  
  export const mutations = {
    pushData(state, new_data) {
        state.data[new_data['type']].push(new_data)
    }
  }